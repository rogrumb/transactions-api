<?php


namespace Tests\Unit;

use App\Enums\TransactionStatuses;
use App\Models\Transaction;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class ShowTransactionTest extends TestCase
{
    use RefreshDatabase;
    /**
     * A basic test example.
     *
     * @return void
     */
    public function test_success()
    {
        $transaction = Transaction::factory()->create();

        $response = $this->getJson($this->getRoute($transaction->id));

        $response->assertStatus(200)
            ->assertJsonStructure([
                'data' => $this->getTransactionSchema()
            ]);
    }

    public function test_failed_with_wrong_transaction_id()
    {
        Transaction::factory()->create();

        $response = $this->getJson($this->getRoute(0));

        $response->assertStatus(404);
    }

    private function getRoute(int $transactionId): string
    {
        return route('api.transactions.show', [$transactionId]);
    }
}
