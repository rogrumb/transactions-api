<?php

namespace Database\Factories;

use App\Enums\TransactionStatuses;
use App\Models\Transaction;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Transaction>
 */
class TransactionFactory extends Factory
{
    protected $model = Transaction::class;
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        return [
            'user_id' => 1,
            'status' => TransactionStatuses::Pending->value,
            'receiver_account' => $this->faker->text(250),
            'receiver_name' => $this->faker->text(250),
            'currency' => 'eur',
            'details' => $this->faker->text(250),
            'amount' => 100
        ];
    }
}
